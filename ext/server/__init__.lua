headshotMultiplier = 5

Hooks:Install('Soldier:Damage', 1, function(hook, soldier, info, giverInfo)
	if (info.boneIndex == 1) then -- headshot
		info.damage = info.damage / 2 * headshotMultiplier -- headshot multiplier is 2x by default
		hook:Pass(soldier, info, giverInfo)
	end
end)